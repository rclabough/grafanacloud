#!/bin/bash

echo JS Directory is $SOURCE_DIR

# creating a zip file and suppressing the output
cd $SOURCE_DIR/js
ls -all
rm -rf node_modules
npm install

BIN_DIR=/var/bin

mkdir $BIN_DIR
zip -r $BIN_DIR/upload.zip *

# version is just an md5 hash
Version=$(md5sum -b /var/bin/upload.zip | awk '{print $1}')

# uploading zip files to S3
echo Uploading to S3
aws s3 cp $BIN_DIR/upload.zip s3://$S3_BUCKET_NAME/ClaboughListener-$Version.zip
aws s3 cp $BIN_DIR/upload.zip s3://$S3_BUCKET_NAME/ClaboughListener-latest.zip
rm $BIN_DIR/upload.zip

echo Submit CFT
aws cloudformation package --template $SOURCE_DIR/cf/main.json --s3-bucket $S3_BUCKET_NAME --s3-prefix cf --output-template-file packaged.json

echo Build Stack if not exists
if ! aws cloudformation describe-stacks --stack-name $STACK_NAME; then
    echo MUST CREATE STACK
    aws cloudformation create-stack --stack-name $STACK_NAME --template-url s3://$S3_BUCKET_NAME/cf/packaged.json
else
    aws cloudformation update-stack --stack-name $STACK_NAME --template-url s3://$S3_BUCKET_NAME/cf/packaged.json
fi




# build.sh should output the version only
echo $Version
