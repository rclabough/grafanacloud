FROM ubuntu

RUN apt-get update
RUN apt-get install -y nodejs npm zip python-pip python-dev build-essential 
RUN pip install --upgrade pip 
RUN pip install --upgrade virtualenv 
RUN pip install awscli
RUN pip install --upgrade awscli


ENV S3_BUCKET_NAME clabough-tech-continuous-delivery
ENV CT_BUILD_DIR /usr/share/setup
ENV SOURCE_DIR /var/source
ENV STACK_NAME claboughtechgrafana

VOLUME /var/cf
VOLUME /var/js

RUN echo $CT_CF

ADD docker-scripts/.aws-config /root/.aws/config
COPY docker-scripts/upload.sh /usr/share/setup/upload.sh
RUN chmod +x /usr/share/setup/upload.sh

CMD $CT_BUILD_DIR/upload.sh
