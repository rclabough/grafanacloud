clean:
	rm -rf js/node_modules

docker:
	sudo docker build -t awscli .

upload: clean
	sudo docker run -it -v $(CURDIR):/var/source awscli

init:
	sudo chmod 777 -R .
